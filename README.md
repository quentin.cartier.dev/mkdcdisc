# mkdcdisc

A command-line tool to generate Dreamcast-bootable .CDI image files for homebrew games.

This intends to be a one-stop-shop for creating Dreamcast DiscJuggler images, doing what scramble, makeip, objcopy, and cdi4dc do currently but with
additional features like automatic disc padding, and CDDA audio tracks.

## Usage

```
mkdcdisc --help

Usage: mkdcdisc [OPTION]... -e [EXECUTABLE] -o [OUTPUT]
Generate a DiscJuggler .cdi file for use with the SEGA Dreamcast.

  -a, --author            author of the disc/game
  -c, --cdda              .wav file to use as an audio track. Specify multiple times to create multiple tracks
  -d, --directory         directory to include (recursively) in the data track. Repeat for multiple directories
  -e, --elf               executable file to use as 1ST_READ.BIN
  -f, --file              file to include in the data track. Repeat for multiple files
  -h, --help              this help screen
  -m, --no-mr             disable the default MR boot image
  -I, --dump-iso          if specified, the data track will be written to a .iso alongside the .cdi
  -o, --output            output filename
  -n, --name              name of the game (must be fewer than 128 characters)
  -N, --no-padding        specify to disable padding of the data track
  -q, --quiet             disable logging. equivalent to 'v 0'
  -r, --release           release date in YYYYMMDD format
  -v, --verbosity         a number between 0 and 3, 0 == no output
```
## Dependencies
- A C++ Compiler
- git
- meson
- libisofs

Fedora: `dnf install git meson gcc gcc-c++ libisofs-devel`

Ubuntu: `sudo apt install git meson build-essential pkg-config libisofs-dev`

MacOS: `brew install git meson pkg-config libisofs`

## Download and Build
```
# clone source
git clone https://gitlab.com/simulant/mkdcdisc
cd mkdcdisc

meson setup builddir
meson compile -C builddir

# run
./builddir/mkdcdisc -h
```

